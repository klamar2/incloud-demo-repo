import requests

def foo(a):
    result = a ** 2
    print(f"{a} ** 2 = {result}")
    return result

def bar(a):
    result = a + 3
    print(f"{a} + 3 = {result}")
    return result

def get_product(product_id: int):
    response = requests.get(f"https://api.predic8.de/shop/products/{product_id}")
    response.raise_for_status()
    return response.json()

def get_vendor(vendor_id: int):
    response = requests.get(f"https://api.predic8.de/shop/vendors/{vendor_id}")
    response.raise_for_status()
    return response.json()

def slack_notify(webhook_url, text):
    response = requests.post(webhook_url, json={"text": text})
    response.raise_for_status()
    return response.status_code
